<?php

namespace Tests\AppBundle\Controller\API;

class UsersPutTest extends BaseApiTest
{
    /**
     * Set up
     */
    public function setUp()
    {
        parent::setUp();
    }

    /**
     * User exist
     */
    public function testUserExistAction()
    {
        $input = array(
            'email' => 'new_lex_email@example.com',
            'firstName' => 'new_lex_first_name',
            'lastName' => 'new_lex_last_name',
            'group' => 3,
        );

        $crawler = $this->client->request('PUT', '/users/1/', $input);
        $this->assertEquals('AppBundle\Controller\API\UserController::usersPutAction', $this->client->getRequest()->attributes->get('_controller'));
        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals($content['status'], 'success');

        $crawler = $this->client->request('GET', '/users/1/');
        $this->assertEquals('AppBundle\Controller\API\UserController::userGetAction', $this->client->getRequest()->attributes->get('_controller'));
        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals($content['status'], 'success');

        $this->assertEquals($content['data']['email'], 'new_lex_email@example.com');
        $this->assertEquals($content['data']['firstName'], 'new_lex_first_name');
        $this->assertEquals($content['data']['lastName'], 'new_lex_last_name');
    }

    /**
     * User not exist
     */
    public function testUserNotExistAction()
    {
        $input = array(
            'email' => 'not_found@example.com',
        );

        $crawler = $this->client->request('PUT', '/users/5/', $input);
        $this->assertEquals('AppBundle\Controller\API\UserController::usersPutAction', $this->client->getRequest()->attributes->get('_controller'));

        $content = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertEquals($content['status'], 'error');
        $this->assertEquals($content['code'], 6);
        $this->assertEquals($content['message'], 'User is not found');
    }

    /**
     * User non active
     */
    public function testUserNonActiveAction()
    {
        $input = array(
            'email' => 'new_bob_email@example.com',
            'firstName' => 'new_bob_first_name',
            'lastName' => 'new_bob_last_name',
            'state' => true,
        );

        $crawler = $this->client->request('PUT', '/users/4/', $input);
        $this->assertEquals('AppBundle\Controller\API\UserController::usersPutAction', $this->client->getRequest()->attributes->get('_controller'));
        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals($content['status'], 'success');

        $crawler = $this->client->request('GET', '/users/4/');
        $this->assertEquals('AppBundle\Controller\API\UserController::userGetAction', $this->client->getRequest()->attributes->get('_controller'));
        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals($content['status'], 'success');

        $this->assertEquals($content['data']['email'], 'new_bob_email@example.com');
        $this->assertEquals($content['data']['firstName'], 'new_bob_first_name');
        $this->assertEquals($content['data']['lastName'], 'new_bob_last_name');
    }
}
