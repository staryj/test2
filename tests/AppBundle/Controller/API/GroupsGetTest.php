<?php

namespace Tests\AppBundle\Controller\API;

class GroupsGetTest extends BaseApiTest
{
    /**
     * Set up
     */
    public function setUp()
    {
        parent::setUp();
    }

    /**
     * Groups exist
     */
    public function testGroupsExistAction()
    {
        $crawler = $this->client->request('GET', '/groups/');
        $this->assertEquals('AppBundle\Controller\API\GroupController::groupsGetAction', $this->client->getRequest()->attributes->get('_controller'));

        $content = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertEquals($content['status'], 'success');
        $this->assertTrue(is_array($content['data']));

        $groupUsers = $content['data'][0];
        $this->assertEquals($groupUsers['id'], 1);
        $this->assertEquals($groupUsers['name'], 'users');

        $groupAdmins = $content['data'][1];
        $this->assertEquals($groupAdmins['id'], 2);
        $this->assertEquals($groupAdmins['name'], 'admins');

        $groupModerators = $content['data'][2];
        $this->assertEquals($groupModerators['id'], 3);
        $this->assertEquals($groupModerators['name'], 'moderators');
    }
}
