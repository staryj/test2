<?php

namespace Tests\AppBundle\Controller\API;

class GroupsPostTest extends BaseApiTest
{
    /**
     * Set up
     */
    public function setUp()
    {
        parent::setUp();
    }

    /**
     * With name
     */
    public function testWithAllParamsAction()
    {
        $input = array(
            'name' => 'anonymous',
        );

        $crawler = $this->client->request('POST', '/groups/', $input);
        $this->assertEquals('AppBundle\Controller\API\GroupController::groupsPostAction', $this->client->getRequest()->attributes->get('_controller'));

        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals($content['status'], 'success');
    }

    /**
     * Empty name
     */
    public function testEmptyNameAction()
    {
        $crawler = $this->client->request('POST', '/groups/');
        $this->assertEquals('AppBundle\Controller\API\GroupController::groupsPostAction', $this->client->getRequest()->attributes->get('_controller'));

        $content = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertEquals($content['status'], 'error');
        $this->assertEquals($content['code'], 1);
        $this->assertEquals($content['message'], 'Missing parameter "name"');
    }

    /**
     * Group already exist
     */
    public function testGroupAlreadyExistAction()
    {
        $input = array(
            'name' => 'users',
        );

        $crawler = $this->client->request('POST', '/groups/', $input);
        $this->assertEquals('AppBundle\Controller\API\GroupController::groupsPostAction', $this->client->getRequest()->attributes->get('_controller'));

        $content = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertEquals($content['status'], 'error');
        $this->assertEquals($content['code'], 3);
        $this->assertEquals($content['message'], 'Group "users" is already exists');
    }
}
