<?php

namespace Tests\AppBundle\Controller\API;

class UsersPostTest extends BaseApiTest
{
    /**
     * Set up
     */
    public function setUp()
    {
        parent::setUp();
    }

    /**
     * With all params
     */
    public function testWithAllParamsAction()
    {
        $input = array(
            'email' => 'new@example.com',
            'firstName' => 'new_first_name',
            'lastName' => 'new_last_name',
            'group' => 3,
        );

        $crawler = $this->client->request('POST', '/users/', $input);
        $this->assertEquals('AppBundle\Controller\API\UserController::usersPostAction', $this->client->getRequest()->attributes->get('_controller'));

        $content = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertEquals($content['status'], 'success');
    }

    /**
     * Empty email
     */
    public function testEmptyEmailAction()
    {
        $crawler = $this->client->request('POST', '/users/');
        $this->assertEquals('AppBundle\Controller\API\UserController::usersPostAction', $this->client->getRequest()->attributes->get('_controller'));

        $content = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertEquals($content['status'], 'error');
        $this->assertEquals($content['code'], 1);
        $this->assertEquals($content['message'], 'Missing parameter "email"');
    }

    /**
     * User already exist
     */
    public function testUserAlreadyExistAction()
    {
        $input = array(
            'email' => 'lex@example.com',
        );

        $crawler = $this->client->request('POST', '/users/', $input);
        $this->assertEquals('AppBundle\Controller\API\UserController::usersPostAction', $this->client->getRequest()->attributes->get('_controller'));

        $content = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertEquals($content['status'], 'error');
        $this->assertEquals($content['code'], 5);
        $this->assertEquals($content['message'], 'User "lex@example.com" is already exists');
    }

    /**
     * User already exist, but non active
     */
    public function testUserAlreadyExistNonActiveAction()
    {
        $input = array(
            'email' => 'bob@example.com',
        );

        $crawler = $this->client->request('POST', '/users/', $input);
        $this->assertEquals('AppBundle\Controller\API\UserController::usersPostAction', $this->client->getRequest()->attributes->get('_controller'));

        $content = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertEquals($content['status'], 'error');
        $this->assertEquals($content['code'], 5);
        $this->assertEquals($content['message'], 'User "bob@example.com" is already exists');
    }
}
