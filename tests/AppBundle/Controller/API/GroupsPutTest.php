<?php

namespace Tests\AppBundle\Controller\API;

class GroupsPutTest extends BaseApiTest
{
    /**
     * Set up
     */
    public function setUp()
    {
        parent::setUp();
    }

    /**
     * Group exist
     */
    public function testGroupExistAction()
    {
        $input = array(
            'name' => 'users_new_name',
        );

        $crawler = $this->client->request('PUT', '/groups/1/', $input);
        $this->assertEquals('AppBundle\Controller\API\GroupController::groupsPutAction', $this->client->getRequest()->attributes->get('_controller'));
        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals($content['status'], 'success');

        $crawler = $this->client->request('GET', '/groups/');
        $this->assertEquals('AppBundle\Controller\API\GroupController::groupsGetAction', $this->client->getRequest()->attributes->get('_controller'));

        $content = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertEquals($content['status'], 'success');
        $this->assertTrue(is_array($content['data']));

        $groupUsers = $content['data'][0];
        $this->assertEquals($groupUsers['id'], 1);
        $this->assertEquals($groupUsers['name'], 'users_new_name');
    }

    /**
     * User not exist
     */
    public function testGroupNotExistAction()
    {
        $input = array(
            'name' => 'not_found',
        );

        $crawler = $this->client->request('PUT', '/groups/5/', $input);
        $this->assertEquals('AppBundle\Controller\API\GroupController::groupsPutAction', $this->client->getRequest()->attributes->get('_controller'));

        $content = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertEquals($content['status'], 'error');
        $this->assertEquals($content['code'], 4);
        $this->assertEquals($content['message'], 'Group is not found');
    }
}
