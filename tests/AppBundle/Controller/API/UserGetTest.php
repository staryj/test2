<?php

namespace Tests\AppBundle\Controller\API;

class UserGetTest extends BaseApiTest
{
    /**
     * Set up
     */
    public function setUp()
    {
        parent::setUp();
    }

    /**
     * User exist
     */
    public function testUserExistAction()
    {
        $crawler = $this->client->request('GET', '/users/1/');
        $this->assertEquals('AppBundle\Controller\API\UserController::userGetAction', $this->client->getRequest()->attributes->get('_controller'));

        $content = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertEquals($content['status'], 'success');
        $this->assertTrue(is_array($content['data']));
        $this->assertEquals($content['data']['id'], 1);
        $this->assertEquals($content['data']['email'], 'lex@example.com');
        $this->assertEquals($content['data']['firstName'], 'lex_first_name');
        $this->assertEquals($content['data']['lastName'], 'lex_last_name');
        $this->assertTrue(is_array($content['data']['group']));
    }

    /**
     * User not exist
     */
    public function testUserNotExistAction()
    {
        $crawler = $this->client->request('GET', '/users/5/');
        $this->assertEquals('AppBundle\Controller\API\UserController::userGetAction', $this->client->getRequest()->attributes->get('_controller'));

        $content = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertEquals($content['status'], 'error');
        $this->assertEquals($content['code'], 6);
        $this->assertEquals($content['message'], 'User is not found');
    }

    /**
     * User non active
     */
    public function testUserNonActiveAction()
    {
        $crawler = $this->client->request('GET', '/users/4/');
        $this->assertEquals('AppBundle\Controller\API\UserController::userGetAction', $this->client->getRequest()->attributes->get('_controller'));

        $content = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertEquals($content['status'], 'error');
        $this->assertEquals($content['code'], 6);
        $this->assertEquals($content['message'], 'User is not found');
    }
}
