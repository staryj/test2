<?php

namespace Tests\AppBundle\Controller\API;

class UsersGetTest extends BaseApiTest
{
    /**
     * Set up
     */
    public function setUp()
    {
        parent::setUp();
    }

    /**
     * Users exist
     */
    public function testUsersExistAction()
    {
        $crawler = $this->client->request('GET', '/users/');
        $this->assertEquals('AppBundle\Controller\API\UserController::usersGetAction', $this->client->getRequest()->attributes->get('_controller'));

        $content = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertEquals($content['status'], 'success');
        $this->assertTrue(is_array($content['data']));

        $userLex = $content['data'][0];
        $this->assertEquals($userLex['id'], 1);
        $this->assertEquals($userLex['email'], 'lex@example.com');
        $this->assertEquals($userLex['firstName'], 'lex_first_name');
        $this->assertEquals($userLex['lastName'], 'lex_last_name');
        $this->assertTrue(is_array($userLex['group']));
    }

    /**
     * Without non active
     */
    public function testWithoutNonActiveAction()
    {
        $crawler = $this->client->request('GET', '/users/');
        $this->assertEquals('AppBundle\Controller\API\UserController::usersGetAction', $this->client->getRequest()->attributes->get('_controller'));

        $content = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertEquals($content['status'], 'success');
        $this->assertTrue(is_array($content['data']));

        $this->assertTrue(isset($content['data'][0]));
        $this->assertTrue(isset($content['data'][1]));
        $this->assertTrue(isset($content['data'][2]));
        $this->assertFalse(isset($content['data'][3]));
    }
}
