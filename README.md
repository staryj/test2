POST /users/        create a user       Parameters {email, firstName, lastName, state, group}       Response {status:success}

PUT /users/{id}/    modify user info    Parameters {email, firstName, lastName, state, group}       Response {status:success}

GET /users/         fetch list of users                                                             Response {status:success,data:[{id,email,firstName,lastName,creationDate,group:{id,name}]}

GET /users/{id}/    fetch info of a user                                                            Response {status:success,data:{id,email,firstName,lastName,creationDate,group:{id,name}}





POST /groups/        create a group         Parameters {name}       Response {status:success}

PUT /groups/{id}/    modify group info      Parameters {name}       Response {status:success}

GET /groups/         fetch list of groups                           Response {status:success,data:[{id,email,firstName,lastName,creationDate,group:{id,name}]}
