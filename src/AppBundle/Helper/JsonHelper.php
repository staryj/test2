<?php

namespace AppBundle\Helper;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Translation\DataCollectorTranslator;
use Symfony\Component\Translation\IdentityTranslator;

class JsonHelper
{
    const ERROR_MISSING_PARAMETERS = 1;
    const ERROR_INVALID_PARAMETERS = 2;
    const ERROR_GROUP_ALREADY_EXISTS = 3;
    const ERROR_GROUP_NOT_FOUND = 4;
    const ERROR_USER_ALREADY_EXISTS = 5;
    const ERROR_USER_NOT_FOUND = 6;
    const ERROR_UNKNOWN_ERROR = 7;

    private $translator;

    public function __construct(DataCollectorTranslator $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param array $errors
     * @return JsonResponse
     */
    public function error($errors)
    {
        $result = array();
        foreach ($errors as $error) {
            $result[] = array(
                'code' => $error,
                'message' => $this->translator->trans($error),
            );
        }

        return $this->json(
            array(
                'status' => 'error',
                'errors' => $result,
            ),
            Response::HTTP_BAD_REQUEST
        );
    }

    /**
     * @param array $parameters
     * @return JsonResponse
     */
    public function success($parameters = array())
    {
        $parameters['status'] = 'success';
        return $this->json($parameters, Response::HTTP_OK);
    }

    /**
     * @param array $data
     * @param integer $status
     * @param array $headers
     * @return JsonResponse
     */
    public function json($data = array(), $status = 200, array $headers = array())
    {
        $response = new JsonResponse($data, $status, $headers);
        $response->setEncodingOptions(JSON_UNESCAPED_SLASHES);
        return $response;
    }
}
