<?php

namespace AppBundle\Serializer\Normalizer;

use AppBundle\Entity\Group;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * Groups normalizer
 */
class GroupsNormalizer implements NormalizerInterface
{
    protected $groupNormalizer;

    public function __construct(GroupNormalizer $groupNormalizer)
    {
        $this->groupNormalizer = $groupNormalizer;
    }

    /**
     * {@inheritdoc}
     */
    public function normalize($objects, $format = null, array $context = array())
    {
        return array_map(
            function ($object) {
                return $this->groupNormalizer->normalize($object);
            },
            $objects
        );
    }

    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null)
    {
        return is_array($data);
    }
}