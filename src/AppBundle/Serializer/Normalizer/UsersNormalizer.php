<?php

namespace AppBundle\Serializer\Normalizer;

use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * Users normalizer
 */
class UsersNormalizer implements NormalizerInterface
{
    protected $userNormalizer;

    public function __construct(UserNormalizer $userNormalizer)
    {
        $this->userNormalizer = $userNormalizer;
    }

    /**
     * {@inheritdoc}
     */
    public function normalize($objects, $format = null, array $context = array())
    {
        return array_map(
            function ($object) use ($format, $context) {
                return $this->userNormalizer->normalize($object, $format, $context);
            },
            $objects
        );
    }

    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null)
    {
        return is_array($data);
    }
}