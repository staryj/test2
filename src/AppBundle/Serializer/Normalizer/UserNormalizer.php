<?php

namespace AppBundle\Serializer\Normalizer;

use AppBundle\Entity\User;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * User normalizer
 */
class UserNormalizer implements NormalizerInterface
{
    protected $groupNormalizer;

    public function __construct(GroupNormalizer $groupNormalizer)
    {
        $this->groupNormalizer = $groupNormalizer;
    }

    /**
     * {@inheritdoc}
     */
    public function normalize($object, $format = null, array $context = array())
    {
        return array(
            'id' => $object->getId(),
            'email' => $object->getEmail(),
            'firstName' => $object->getFirstName(),
            'lastName' => $object->getLastName(),
            'creationDate' => $object->getCreationDate()->getTimestamp(),
            'group' => $this->groupNormalizer->normalize($object->getGroup()),
        );
    }

    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof User;
    }
}