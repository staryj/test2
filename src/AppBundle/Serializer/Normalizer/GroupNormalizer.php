<?php

namespace AppBundle\Serializer\Normalizer;

use AppBundle\Entity\Group;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * Group normalizer
 */
class GroupNormalizer implements NormalizerInterface
{
    /**
     * {@inheritdoc}
     */
    public function normalize($object, $format = null, array $context = array())
    {
        if(is_null($object)) {
            return null;
        }
        return array(
            'id'   => $object->getId(),
            'name' => $object->getName(),
        );
    }

    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof Group;
    }
}