<?php

namespace AppBundle\Controller\API;

use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use AppBundle\Helper\JsonHelper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class UserController extends BaseApiController
{
    /**
     * Create
     *
     * @param $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *
     * @Route("/users/")
     * @Method({"POST"})
     */
    public function usersPostAction(Request $request)
    {
        $data = $request->request->all();
        $form = $this->createForm(UserType::class, new User(), array('csrf_protection' => false));
        $form->submit($data);

        if ($form->isValid()) {
            $user = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            return $this->get('json.helper')->success();
        }

        $errors = $this->getErrors($form);
        return $this->get('json.helper')->error($errors);
    }

    /**
     * Modify
     *
     * @param $request
     * @param integer $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *
     * @Route("/users/{id}/",requirements={"page": "\d+"})
     * @Method({"PUT"})
     */
    public function usersPutAction(Request $request, $id)
    {
        $user = $this->getDoctrine()->getRepository('AppBundle:User')->findOneBy(array('id' => $id));

        if (is_null($user)) {
            return $this->get('json.helper')->error(array(JsonHelper::ERROR_USER_NOT_FOUND));
        }

        // Если это не написать, то текущие значения полей, для которых не пришли данные, заменятся на NULL

        $data = $request->request->all();
        
        if (!isset($data['group'])) {
            $data['group'] = $user->getGroup();
        }
        if (!isset($data['email'])) {
            $data['email'] = $user->getEmail();
        }
        if (!isset($data['firstName'])) {
            $data['firstName'] = $user->getFirstName();
        }
        if (!isset($data['lastName'])) {
            $data['lastName'] = $user->getLastName();
        }
        if (!isset($data['state'])) {
            $data['state'] = $user->getState();
        }

        $form = $this->createForm(UserType::class, $user, array('csrf_protection' => false));
        $form->submit($data);

        if ($form->isValid()) {
            $user = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            return $this->get('json.helper')->success();
        }

        $errors = $this->getErrors($form);
        return $this->get('json.helper')->error($errors);
    }

    /**
     * Get list
     *
     * @param $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *
     * @Route("/users/")
     * @Method({"GET"})
     */
    public function usersGetAction(Request $request)
    {
        $users = $this->getDoctrine()->getRepository('AppBundle:User')->findBy(array('state' => true), array('id' => 'ASC'));

        $result = $this->get('normalizer.users')->normalize($users);
        return $this->get('json.helper')->success(array('data' => $result));
    }

    /**
     * Get one
     *
     * @param $request
     * @param integer $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *
     * @Route("/users/{id}/")
     * @Method({"GET"})
     */
    public function userGetAction(Request $request, $id)
    {
        $user = $this->getDoctrine()->getRepository('AppBundle:User')->findOneBy(array('id' => $id, 'state' => true));
        if (is_null($user)) {
            return $this->get('json.helper')->error(array(JsonHelper::ERROR_USER_NOT_FOUND));
        }

        $result = $this->get('normalizer.user')->normalize($user);
        return $this->get('json.helper')->success(array('data' => $result));
    }
}
