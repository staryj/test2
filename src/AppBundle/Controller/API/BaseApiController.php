<?php

namespace AppBundle\Controller\API;

use AppBundle\Helper\JsonHelper;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BaseApiController extends Controller
{
    protected function getErrors($form)
    {
        $errors = $form->getErrors(true);
        $errorMessages = array();
        foreach ($errors as $error) {
            $errorMessages[] = $error->getMessage();
        }
        $result = array();
        if (in_array(JsonHelper::ERROR_MISSING_PARAMETERS, $errorMessages)) {
            $result[] = JsonHelper::ERROR_MISSING_PARAMETERS;
        }
        if (in_array(JsonHelper::ERROR_INVALID_PARAMETERS, $errorMessages)) {
            $result[] = JsonHelper::ERROR_INVALID_PARAMETERS;
        }
        if (in_array(JsonHelper::ERROR_GROUP_ALREADY_EXISTS, $errorMessages)) {
            $result[] = JsonHelper::ERROR_GROUP_ALREADY_EXISTS;
        }
        if (in_array(JsonHelper::ERROR_GROUP_NOT_FOUND, $errorMessages)) {
            $result[] = JsonHelper::ERROR_GROUP_NOT_FOUND;
        }
        if (in_array(JsonHelper::ERROR_USER_ALREADY_EXISTS, $errorMessages)) {
            $result[] = JsonHelper::ERROR_USER_ALREADY_EXISTS;
        }
        if (in_array(JsonHelper::ERROR_USER_NOT_FOUND, $errorMessages)) {
            $result[] = JsonHelper::ERROR_USER_NOT_FOUND;
        }

        if(count($result) === 0) {
            $result[] = JsonHelper::ERROR_UNKNOWN_ERROR;
        }
        return $result;
    }
}
