<?php

namespace AppBundle\Controller\API;

use AppBundle\Entity\Group;
use AppBundle\Form\GroupType;
use AppBundle\Helper\JsonHelper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class GroupController extends BaseApiController
{
    /**
     * Create
     *
     * @param $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *
     * @Route("/groups/")
     * @Method({"POST"})
     */
    public function groupsPostAction(Request $request)
    {
        $data = $request->request->all();
        $form = $this->createForm(GroupType::class, new Group(), array('csrf_protection' => false));
        $form->submit($data);

        if ($form->isValid()) {
            $group = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($group);
            $em->flush();
            return $this->get('json.helper')->success();
        }

        $errors = $this->getErrors($form);
        return $this->get('json.helper')->error($errors);
    }

    /**
     * Modify
     *
     * @param $request
     * @param integer $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *
     * @Route("/groups/{id}/",requirements={"page": "\d+"})
     * @Method({"PUT"})
     */
    public function groupsPutAction(Request $request, $id)
    {
        $group = $this->getDoctrine()->getRepository('AppBundle:Group')->find($id);
        if (is_null($group)) {
            return $this->get('json.helper')->error(array(JsonHelper::ERROR_GROUP_NOT_FOUND));
        }

        $data = $request->request->all();
        $form = $this->createForm(GroupType::class, $group, array('csrf_protection' => false));
        $form->submit($data);

        if ($form->isValid()) {
            $group = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($group);
            $em->flush();
            return $this->get('json.helper')->success();
        }

        $errors = $this->getErrors($form);
        return $this->get('json.helper')->error($errors);
    }

    /**
     * Get list
     *
     * @param $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *
     * @Route("/groups/")
     * @Method({"GET"})
     */
    public function groupsGetAction(Request $request)
    {
        $groups = $this->getDoctrine()->getRepository('AppBundle:Group')->findBy(array(), array('id' => 'ASC'));

        $result = $this->get('normalizer.groups')->normalize($groups);
        return $this->get('json.helper')->success(array('data' => $result));
    }
}
