<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Group;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class GroupFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $em)
    {
        $group = new Group();
        $group->setName('users');
        $em->persist($group);
        $this->addReference('group_users', $group);

        $group = new Group();
        $group->setName('admins');
        $em->persist($group);
        $this->addReference('group_admins', $group);

        $group = new Group();
        $group->setName('moderators');
        $em->persist($group);
        $this->addReference('group_moderators', $group);

        $em->flush();
    }

    function getOrder()
    {
        return 1;
    }
}