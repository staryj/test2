<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $em)
    {
        $user = new User();
        $user->setEmail('lex@example.com');
        $user->setFirstName('lex_first_name');
        $user->setLastName('lex_last_name');
        $user->setGroup($this->getReference('group_users'));
        $em->persist($user);
        $this->addReference('user_1', $user);

        $user = new User();
        $user->setEmail('max@example.com');
        $user->setFirstName('max_first_name');
        $user->setLastName('max_last_name');
        $user->setGroup($this->getReference('group_users'));
        $em->persist($user);
        $this->addReference('user_2', $user);

        $user = new User();
        $user->setEmail('nick@example.com');
        $user->setFirstName('nick_first_name');
        $user->setLastName('nick_last_name');
        $user->setGroup($this->getReference('group_admins'));
        $em->persist($user);
        $this->addReference('user_3', $user);

        $user = new User();
        $user->setEmail('bob@example.com');
        $user->setFirstName('bob_first_name');
        $user->setLastName('bob_last_name');
        $user->setGroup($this->getReference('group_admins'));
        $user->setState(false);
        $em->persist($user);
        $this->addReference('user_4', $user);

        $em->flush();
    }

    function getOrder()
    {
        return 2;
    }
}